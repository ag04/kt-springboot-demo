package com.ag04.ktdemo.model
import org.hibernate.annotations.Type
import org.joda.time.LocalDate
import javax.persistence.*

/**
 *
 * @author dmadunic on 01.05.2018
 */
@Entity
@Table(name="EXPENSE")
data class Expense (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "ID", nullable = false, unique = true) var id: Long?,
        @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
        @Column(name = "EXPENSE_DATE", nullable = false) var expenseDate: LocalDate?,
        @Column(name = "AMOUNT", nullable = false) var amount: Double?,
        @Column(name = "AMOUNT_TAX", nullable = false) var amountTax: Double?,
        @Column(name = "RECEIVER", nullable = false) var receiver: String?,
        @Column(name = "EXPENSE_DESC", nullable = false) var description: String?,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "EXPENSES_TYPE_ID", referencedColumnName = "ID") var type: ExpensesType?
) {
        constructor() : this(null, null, null, null, null, null, null)
}


/*

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "ID", nullable = false, unique = true) var id: Long? = null,
        @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
        @Column(name = "EXPENSE_DATE", nullable = false) var expenseDate: LocalDate? = null,
        @Column(name = "AMOUNT", nullable = false) var amount: Double? = null,
        @Column(name = "AMOUNT_TAX", nullable = false) var amountTax: Double? = null,
        @Column(name = "RECEIVER", nullable = false) var receiver: String? = null,
        @Column(name = "EXPENSE_DESC", nullable = false) var description: String? = null,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "EXPENSES_TYPE_ID", referencedColumnName = "ID") var type: ExpensesType? = null


 */