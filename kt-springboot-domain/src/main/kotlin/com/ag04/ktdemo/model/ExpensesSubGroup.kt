package com.ag04.ktdemo.model

import javax.persistence.*

/**
 *
 * @author dmadunic on 30.04.2018
 */
@Entity
@Table(name="EXPENSES_SUBGROUP")
data class ExpensesSubGroup (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true) var id: Long? = null,
    @Column(name = "NAME", nullable = false) var name: String? = null,
    @Column(name = "CODE", nullable = false) var code: String? = null,
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "EXPENSES_GROUP_ID", referencedColumnName = "ID") var group: ExpensesGroup? = null
)