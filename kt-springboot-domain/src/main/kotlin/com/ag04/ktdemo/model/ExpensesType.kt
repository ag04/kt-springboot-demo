package com.ag04.ktdemo.model

import javax.persistence.*

/**
 *
 * @author dmadunic on 30.04.2018
 */
@Entity
@Table(name="EXPENSES_TYPE")
data class ExpensesType (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "ID", nullable = false, unique = true) var id: Long?,
        @Column(name = "NAME", nullable = false) var name: String?,
        @Column(name = "CODE", nullable = false) var code: String?,
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "EXPENSES_SUBGROUP_ID", referencedColumnName = "ID") var subGroup: ExpensesSubGroup? = null
) {
        constructor() : this(null, null, null, null)
}
