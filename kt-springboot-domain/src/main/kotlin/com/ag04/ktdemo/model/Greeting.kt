package com.ag04.ktdemo.model

/**
 *
 * Created by: dmadunic on 27/04/18<br/>
 */

data class Greeting(val id: Long, val content: String)