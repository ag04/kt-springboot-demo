package com.ag04.ktdemo.model

import javax.persistence.*

/**
 *
 * @author dmadunic on 29.04.2018
 */
@Entity
@Table(name="EXPENSES_GROUP")
data class ExpensesGroup(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "ID", nullable = false, unique = true) var id: Long?,
        @Column(name = "NAME", nullable = false) var name: String?,
        @Column(name = "CODE", nullable = false) var code: String?
) /*{
        constructor() : this(null, null, null)
}*/