package com.ag04.ktdemo.service

import com.ag04.ktdemo.model.ExpensesGroup
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 *
 * @author dmadunic on 29.04.2018
 */
interface ExpensesGroupService {

    fun getById(id: Long): ExpensesGroup

    fun findById(id: Long): ExpensesGroup?

    fun findAll(pageable: Pageable): Page<ExpensesGroup>

}