package com.ag04.ktdemo.service

import com.ag04.ktdemo.model.ExpensesType
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 *
 * @author dmadunic on 30.04.2018
 */
interface ExpensesTypeService {

    fun getById(id: Long): ExpensesType

    fun findById(id: Long): ExpensesType?

    fun findAll(pageable: Pageable): Page<ExpensesType>

    fun exists(id: Long): Boolean

}