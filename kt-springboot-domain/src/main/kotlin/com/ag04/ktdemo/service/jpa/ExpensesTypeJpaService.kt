package com.ag04.ktdemo.service.jpa

import com.ag04.common.exception.EntityNotFoundException
import com.ag04.ktdemo.model.ExpensesType
import com.ag04.ktdemo.repository.ExpensesTypeRepository
import com.ag04.ktdemo.service.ExpensesTypeService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import javax.transaction.Transactional

/**
 *
 * @author dmadunic on 30.04.2018
 */
@Service
@Transactional
internal class ExpensesTypeJpaService(private val expensesTypeRepository: ExpensesTypeRepository) : ExpensesTypeService {

    override fun getById(id: Long): ExpensesType {
        val group = expensesTypeRepository.findOne(id)
        return group ?: throw EntityNotFoundException(id.toString())
    }

    override fun findById(id: Long): ExpensesType? = expensesTypeRepository.findOne(id)

    override fun findAll(pageable: Pageable): Page<ExpensesType> = expensesTypeRepository.findAll(pageable)

    override fun exists(id: Long): Boolean = expensesTypeRepository.exists(id)

}