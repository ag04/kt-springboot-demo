package com.ag04.ktdemo.service

import com.ag04.ktdemo.model.ExpensesSubGroup
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 *
 * @author domagoj on 01.05.2018
 */
interface ExpensesSubGroupService {

    fun getById(id: Long): ExpensesSubGroup

    fun findById(id: Long): ExpensesSubGroup?

    fun findAll(pageable: Pageable): Page<ExpensesSubGroup>

}