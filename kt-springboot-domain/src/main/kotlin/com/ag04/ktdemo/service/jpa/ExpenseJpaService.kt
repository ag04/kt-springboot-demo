package com.ag04.ktdemo.service.jpa

import com.ag04.common.exception.EntityNotFoundException
import com.ag04.ktdemo.model.Expense
import com.ag04.ktdemo.repository.ExpenseRepository
import com.ag04.ktdemo.service.ExpenseService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import javax.transaction.Transactional

/**
 * JPA implementation of ExpenseService.
 *
 * @author dmadunic on 01.05.2018
 */
@Service
@Transactional
internal class ExpenseJpaService(private val expenseRepository : ExpenseRepository) : ExpenseService {

    /**
     * Returns Expense by id, if no record with this id exists EntityNotFoundException is thrown.
     *
     */
    override fun getById(id: Long): Expense {
        val  expense = expenseRepository.findOne(id)
        return expense ?: throw EntityNotFoundException(id.toString())
    }

    /**
     * Returns Expense by id, if no record with this id exists null is returned.
     */
    override fun findById(id: Long): Expense? = expenseRepository.findOne(id)

    override fun findAll(pageable: Pageable): Page<Expense> = expenseRepository.findAll(pageable)

    override fun save(expense: Expense): Expense = expenseRepository.save(expense)

    override fun exist(id: Long): Boolean = expenseRepository.exists(id)

    override fun delete(id: Long) = expenseRepository.delete(id)
}