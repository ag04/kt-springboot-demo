package com.ag04.ktdemo.service

import com.ag04.ktdemo.model.Expense
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 *
 * @author dmadunic on 01.05.2018
 */
interface ExpenseService {

    fun getById(id: Long): Expense

    fun findById(id: Long): Expense?

    fun findAll(pageable: Pageable): Page<Expense>

    fun save(expense: Expense) : Expense

    fun exist(id: Long) : Boolean

    fun delete(id: Long)

}