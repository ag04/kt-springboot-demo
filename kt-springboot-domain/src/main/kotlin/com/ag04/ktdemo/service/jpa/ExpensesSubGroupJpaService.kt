package com.ag04.ktdemo.service.jpa

import com.ag04.common.exception.EntityNotFoundException
import com.ag04.ktdemo.model.ExpensesSubGroup
import com.ag04.ktdemo.repository.ExpensesSubGroupRepository
import com.ag04.ktdemo.service.ExpensesSubGroupService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import javax.transaction.Transactional

/**
 *
 * @author dmadunic on 01.05.2018
 */
@Service
@Transactional
internal class ExpensesSubGroupJpaService(private val expensesSubGroupRepository: ExpensesSubGroupRepository) : ExpensesSubGroupService {

    override fun getById(id: Long): ExpensesSubGroup {
        val subGroup = expensesSubGroupRepository.findOne(id)
        return subGroup ?: throw EntityNotFoundException(id.toString())
    }

    override fun findById(id: Long): ExpensesSubGroup? = expensesSubGroupRepository.findOne(id)

    override fun findAll(pageable: Pageable): Page<ExpensesSubGroup> = expensesSubGroupRepository.findAll(pageable)

}