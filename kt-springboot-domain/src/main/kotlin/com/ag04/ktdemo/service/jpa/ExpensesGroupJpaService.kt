package com.ag04.ktdemo.service.jpa

import com.ag04.common.exception.EntityNotFoundException
import com.ag04.ktdemo.model.ExpensesGroup
import com.ag04.ktdemo.repository.ExpensesGroupRepository
import com.ag04.ktdemo.service.ExpensesGroupService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import javax.transaction.Transactional

/**
 *
 * @author dmadunic on 30.04.2018
 */
@Service
@Transactional
internal class ExpensesGroupJpaService(private val expensesGroupRepository: ExpensesGroupRepository) : ExpensesGroupService {

    override fun getById(id: Long): ExpensesGroup {
        val group = expensesGroupRepository.findOne(id)
        return group ?: throw EntityNotFoundException(id.toString())
    }

    override fun findById(id: Long): ExpensesGroup? = expensesGroupRepository.findOne(id)

    override fun findAll(pageable: Pageable): Page<ExpensesGroup> = expensesGroupRepository.findAll(pageable)
}