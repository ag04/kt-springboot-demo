package com.ag04.ktdemo.repository

import com.ag04.ktdemo.model.Expense
import org.springframework.data.jpa.repository.JpaRepository

/**
 *
 * @author dmadunic on 01.05.2018
 */
interface ExpenseRepository  : JpaRepository<Expense, Long>