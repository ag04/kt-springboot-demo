package com.ag04.ktdemo.repository

import com.ag04.ktdemo.model.ExpensesGroup
import org.springframework.data.jpa.repository.JpaRepository

/**
 *
 * @author dmadunic on 29.04.2018
 */
interface ExpensesGroupRepository : JpaRepository<ExpensesGroup, Long>