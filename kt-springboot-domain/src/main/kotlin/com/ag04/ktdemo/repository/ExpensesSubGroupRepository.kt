package com.ag04.ktdemo.repository

import com.ag04.ktdemo.model.ExpensesSubGroup
import org.springframework.data.jpa.repository.JpaRepository

/**
 *
 * @author dmadunic on 30.04.2018
 */
interface ExpensesSubGroupRepository : JpaRepository<ExpensesSubGroup, Long>