package com.ag04.ktdemo.rest.controller.v1

import com.ag04.common.exception.EntityNotFoundException
import com.ag04.common.exception.ValidationFailedException
import com.ag04.ktdemo.model.Expense
import com.ag04.ktdemo.rest.form.ExpenseForm
import com.ag04.ktdemo.rest.mapper.ExpenseFormMapper
import com.ag04.ktdemo.rest.validator.ExpenseFormValidator
import com.ag04.ktdemo.service.ExpenseService
import com.ag04.support.web.RequestParamUtils
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.validation.BindingResult
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 *
 * @author dmadunic on 01.05.2018
 */
@RestController
@RequestMapping("/v1/expense")
class ExpenseController(
    private val expenseService: ExpenseService,
    private val expenseFormValidator: ExpenseFormValidator,
    private val expenseFormMapper: ExpenseFormMapper
) {
    companion object {
        val LOG = LoggerFactory.getLogger(ExpenseController::class.simpleName)
    }

    @GetMapping("/{id}")
    fun getGroup(@PathVariable(name = "id") id: Long) : Expense = expenseService.getById(id)


    @GetMapping("")
    fun getAll(@RequestParam(value = "page", required = false, defaultValue = "0") page: Int,
               @RequestParam(value = "size", required = false, defaultValue = "50") size: Int,
               @RequestParam(value = "sort", required = false, defaultValue = "expenseDate") sort: String
    ) : Page<Expense> {
        val pageable = RequestParamUtils.pageable(page, size, sort)
        val result : Page<Expense> = expenseService.findAll(pageable)
        return result
    }

    @PostMapping(value = "", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun create(@Valid @RequestBody expenseForm : ExpenseForm, bindingResult : BindingResult) : Expense {
        if (bindingResult.hasErrors()) {
            LOG.error("CREATE finished with validation errors={}", bindingResult)
            throw ValidationFailedException(bindingResult)
        }
        val expense: Expense = expenseFormMapper.mapToEntity(expenseForm, null)
        return expenseService.save(expense)
    }

    @PutMapping(value = "/{id}", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun edit(@PathVariable(name = "id") id: Long, @Valid @RequestBody expenseForm : ExpenseForm, bindingResult : BindingResult) : Expense {
        if (bindingResult.hasErrors()) {
            LOG.error("UPDATE finished with validation errors={}", bindingResult)
            throw ValidationFailedException(bindingResult)
        }
        var expense: Expense = expenseService.getById(id)

        expense = expenseFormMapper.mapToEntity(expenseForm, null, hashMapOf(ExpenseFormMapper.EXPENSE_KEY to expense))

        return expenseService.save(expense)
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun delete(@PathVariable(name = "id") id: Long) {
        if (!expenseService.exist(id)) {
            throw EntityNotFoundException(id.toString())
        }
        expenseService.delete(id)
    }


    @InitBinder("expenseForm")
    fun initProfileFormValidator(dataBinder: WebDataBinder) {
        dataBinder.validator = expenseFormValidator
    }
}