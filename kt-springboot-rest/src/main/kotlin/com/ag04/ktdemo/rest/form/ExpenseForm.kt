package com.ag04.ktdemo.rest.form

import org.joda.time.LocalDate

/**
 *
 * @author dmadunic on 01.05.2018
 */
data class ExpenseForm(
        var id: Long? = null,
        var expenseDate: LocalDate? = null,
        var amount : Double? = null,
        var amountTax : Double? = null,
        var receiver: String? = null,
        var description: String? = null,
        var typeId: Long? = null
)