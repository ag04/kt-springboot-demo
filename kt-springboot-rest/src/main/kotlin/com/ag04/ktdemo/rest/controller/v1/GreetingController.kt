package com.ag04.ktdemo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong
import com.ag04.ktdemo.model.Greeting
import org.springframework.web.bind.annotation.RequestMapping

/**
 *
 * Created by: dmadunic on 27/04/18<br/>
 */
@RestController
@RequestMapping("/v1/greeting")
class GreetingController {

    val counter = AtomicLong()

    @GetMapping("")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String) = Greeting(counter.incrementAndGet(), "Hello, $name")

}