package com.ag04.ktdemo.rest.controller.v1

import com.ag04.ktdemo.model.ExpensesType
import com.ag04.ktdemo.service.ExpensesTypeService
import com.ag04.support.web.RequestParamUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.*

/**
 *
 * @author dmadunic on 30.04.2018
 */
@RestController
@RequestMapping("/v1/expensetype")
class ExpensesTypeController(
    @Autowired private val expensesTypeService: ExpensesTypeService
) {

    @GetMapping("/{id}")
    fun getGroup(@PathVariable(name = "id") id: Long) : ExpensesType = expensesTypeService.getById(id)


    @GetMapping("")
    fun getAll(@RequestParam(value = "page", required = false, defaultValue = "0") page: Int,
               @RequestParam(value = "size", required = false, defaultValue = "50") size: Int,
               @RequestParam(value = "sort", required = false, defaultValue = "name") sort: String
    ) : Page<ExpensesType> {
        val pageable = RequestParamUtils.pageable(page, size, sort)
        val result : Page<ExpensesType> = expensesTypeService.findAll(pageable)
        return result
    }

}