package com.ag04.ktdemo.rest.mapper

import com.ag04.common.mapper.EntityMapper
import com.ag04.ktdemo.model.Expense
import com.ag04.ktdemo.model.ExpensesType
import com.ag04.ktdemo.rest.form.ExpenseForm
import com.ag04.ktdemo.service.ExpensesTypeService
import org.springframework.stereotype.Component
import java.util.*

/**
 *
 * @author dmadunic on 01.05.2018
 */
@Component
class ExpenseFormMapper(val expensesTypeService: ExpensesTypeService) : EntityMapper<Expense, ExpenseForm> {

    override fun mapFromEntity(entity: Expense, locale: Locale?): ExpenseForm {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun mapFromEntity(entity: Expense, locale: Locale?, config: MutableMap<String, Any>?): ExpenseForm {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun mapToEntity(form: ExpenseForm, locale: Locale?): Expense {
        val expense = Expense()
        return mapToEntity(form, locale, hashMapOf(EXPENSE_KEY to expense))
    }

    override fun mapToEntity(form: ExpenseForm, locale: Locale?, config: MutableMap<String, Any>): Expense {
        val expense: Expense = config[EXPENSE_KEY] as Expense
        expense.id = form.id
        expense.amount = form.amount
        expense.amountTax = form.amountTax
        expense.description = form.description
        expense.expenseDate = form.expenseDate
        expense.receiver = form.receiver
        form.typeId?.let {
            val type: ExpensesType? = expensesTypeService.findById(it)
            expense.type = type
        }
        return expense
    }

    companion object {
        const val EXPENSE_KEY = "expense_key"
    }
}