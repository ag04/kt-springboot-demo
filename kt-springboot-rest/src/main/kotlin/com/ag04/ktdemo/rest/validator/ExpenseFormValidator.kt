package com.ag04.ktdemo.rest.validator

import com.ag04.ktdemo.rest.form.ExpenseForm
import com.ag04.ktdemo.service.ExpensesTypeService
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator

/**
 *
 * @author dmadunic on 01.05.2018
 */
@Component
class ExpenseFormValidator(private val expensesTypeService: ExpensesTypeService) : Validator {

    override fun supports(clazz: Class<*>) = ExpenseForm::class.java.isAssignableFrom(clazz)

    override fun validate(target: Any?, errors: Errors) {
        if (target == null) {
            return
        }
        val expenseForm = target as ExpenseForm
        validateExpenseDate(expenseForm, errors)
        validateAmount(expenseForm.amount, "amount", errors)
        validateAmount(expenseForm.amountTax, "amountTax", errors)
        validateReceiver(expenseForm, errors)
        validateDescription(expenseForm, errors)
        validateExpenseType(expenseForm, errors)
    }

    private fun validateExpenseDate(expenseForm: ExpenseForm, errors: Errors) {
        if (expenseForm.expenseDate == null) {
            errors.rejectValue("expenseDate", REQUIRED, "Property expenseDate may not be null")
        }
    }

    private fun validateAmount(amount: Double?, fieldProperty: String, errors: Errors) {
        if (amount == null) {
            errors.rejectValue(fieldProperty, REQUIRED, "Property amount may not be null")
            return
        }

        if (amount < 0.0) {
            errors.rejectValue(fieldProperty, POSITIVE_NUM_REQUIRED, "Property amount may not be less than 0")
        }
    }

    private fun validateReceiver(expenseForm: ExpenseForm, errors: Errors) {
        if (expenseForm.receiver == null) {
            errors.rejectValue("receiver", REQUIRED, "Property receiver may not be null")
            return
        }

        expenseForm.receiver?.let {
            if (it.length > 80) {
                errors.rejectValue("receiver", MAX_LENGTH_80, "Property receiver may not contain more than 80 chars")
            }
        }
    }

    private fun validateDescription(expenseForm: ExpenseForm, errors: Errors) {
        expenseForm.description?.let {
            if (it.length > 255) {
                errors.rejectValue("description", MAX_LENGTH_255, "Property receiver may not contain more than 255 chars")
            }
        }
    }

    private fun validateExpenseType(expenseForm: ExpenseForm, errors: Errors) {
        if (expenseForm.typeId == null) {
            errors.rejectValue("typeId", REQUIRED, "Property typeId may not be null")
        }
        expenseForm.typeId?.let {
            if (!expensesTypeService.exists(it)) {
                errors.rejectValue("typeId", ENTITY_NOT_EXIST, "ExpenseType with id value of $it does not exists")
            }
        }
    }

    companion object {
        const val REQUIRED = "field.required"
        const val POSITIVE_NUM_REQUIRED = "field.positive-value-required"
        const val MAX_LENGTH_80 = "field.max-length.80"
        const val MAX_LENGTH_255 = "field.max-length.255"
        const val ENTITY_NOT_EXIST = "field.no-entity-with-id-exists"
    }

}