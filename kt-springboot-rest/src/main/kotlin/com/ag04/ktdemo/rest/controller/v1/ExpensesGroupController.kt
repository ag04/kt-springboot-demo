package com.ag04.ktdemo.rest

import com.ag04.ktdemo.model.ExpensesGroup
import com.ag04.ktdemo.service.ExpensesGroupService
import com.ag04.support.web.RequestParamUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.*

/**
 *
 * @author dmadunic on 30.04.2018
 */
@RestController
@RequestMapping("/v1/groups")
class ExpensesGroupController(
    @Autowired private val expensesGroupService: ExpensesGroupService
) {

    @GetMapping("/{id}")
    fun getGroup(@PathVariable(name = "id") id: Long) : ExpensesGroup = expensesGroupService.getById(id)

    @GetMapping("")
    fun getAll(@RequestParam(value = "page", required = false, defaultValue = "0") page: Int,
            @RequestParam(value = "size", required = false, defaultValue = "50") size: Int,
            @RequestParam(value = "sort", required = false, defaultValue = "name") sort: String
    ) : Page<ExpensesGroup> {
        val pageable = RequestParamUtils.pageable(page, size, sort)
        val result : Page<ExpensesGroup> = expensesGroupService.findAll(pageable)
        return result
    }

}