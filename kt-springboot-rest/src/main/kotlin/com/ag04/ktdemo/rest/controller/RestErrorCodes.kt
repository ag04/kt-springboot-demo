package com.ag04.ktdemo.rest.controller

/**
 *
 * @author dmadunic on 30.04.2018
 */
object RestErrorCodes {
    const val NOT_FOUND = "error.entity-not-found"

}