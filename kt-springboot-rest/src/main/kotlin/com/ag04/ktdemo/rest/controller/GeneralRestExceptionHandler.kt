package com.ag04.ktdemo.rest.controller

import com.ag04.common.exception.EntityNotFoundException
import com.ag04.common.web.domain.dto.EntityError
import org.slf4j.LoggerFactory
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus

/**
 *
 * @author dmadunic on 30.04.2018
 */
@ControllerAdvice
@Order
class GeneralRestExceptionHandler {
    companion object {
        val LOG = LoggerFactory.getLogger(GeneralRestExceptionHandler::class.simpleName)
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException::class)
    @ResponseBody
    fun handleEntityNotFoundException(ex: EntityNotFoundException): EntityError {
        LOG.info("Caught EntityNotFoundException for entityId=${ex.entityId}")
        val rsp = EntityError()
        //val ex = exception as EntityNotFoundException

        rsp.code = RestErrorCodes.NOT_FOUND
        rsp.message = "No entity with id=${ex.entityId} was found?"
        rsp.entityId = ex.entityId
        return rsp
    }
}