# Kotlin:SpringBoot:Jpa Demo

This file provides instructions how to setup local MSSQL 2017 database running in docker container with persisting data to local (host) volume.

## 1.Prerequistes
* [Docker](https://docs.docker.com/install/)
* [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* Databse of your choice

## 2. Setup (First time)
1. Clone the repository: `git clone https://YourUserName@bitbucket.org/ag04/kt-springboot-demo.git`
2. Run './gradlew clean build' in **/kt-springboot-demo.git** folder
3. Setup database of your choice (MS Sql Serve, Postgres, ...)

## MSSQL 2017 (Linux Setup)
In order to use MS Sql Server as a database for this demo follow the steps bellow.

### 2. Install MS sql command line tools (optional)

 Follow the instructions on this link: [https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools?view=sql-server-linux-2017](https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools?view=sql-server-linux-2017)

 For Ubuntu distro execute:

```bash
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -

curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list

sudo apt-get update 
sudo apt-get install mssql-tools unixodbc-dev
```

Add sql cmd line tools to your path:

```bash
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc
```

### 3. Running mssql docker image

Begin with the basic docker-compose yml file (support/docker/infrastructure/mssql.yml):

```yaml
version: '3.3'
services:
  mssql:
    image: microsoft/mssql-server-linux:2017-latest
    ports:
      - "1433:1433"
    volumes:
      - mssql-volume:/var/opt/mssql
    environment:
      ACCEPT_EULA: "Y"
      MSSQL_SA_PASSWORD: "Secret1#"
volumes:
  mssql-volume:
```

If necessary copy it and change it to suit your needs (password, ports, ...).


**Finally, run docker mssql by issuing the following command:**

```bash
docker-compose -f mssql.yml up
```

### 4. Setup AG04 Database (first time)

Once the docker image is running, connect to it using sqlcmd (you will be prompted for password):

```bash
sqlcmd -S localhost -U SA
```

Now create standard AG04 Database

```sql
CREATE DATABASE AG04;
USE AG04;
GO
```

### 5. Crate application schema and corresponding user
 
Once You have default database you can create schema/user for your applications.

In the following section we will create application user '**ktdemo**' with password '**Ktdemo12**' and schema named '**ktdemo**' that this user will be owner of.

Connect to database using client of your choice (sqlcmd, dbeaver, ...) with SA user and credentials, and execute the following commands:

```sql
CREATE LOGIN ktdemo WITH PASSWORD='Ktdemo12';
CREATE USER ktdemo FOR LOGIN ktdemo;
CREATE SCHEMA ktdemo AUTHORIZATION ktdemo;
ALTER USER ktdemo WITH DEFAULT_SCHEMA=ktdemo;
GRANT CONNECT to ktdemo;
GRANT ALL to ktdemo;
```
Now test connection by connecting with the newly created user.

```bash
sqlcmd -S localhost -d AG04 -U ktdemo
```

## Intellij Idea Setup

### Import project

### Adjust run configuration

## Running rest service



## Credits
* Domagoj Madunić

